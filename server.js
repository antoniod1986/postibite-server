const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const resolvers = require('./resolvers');

const app = express();
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));

// Import Environment Variables and Mongoose Models
require('dotenv').config({ path: 'variables.env' });

const connectDB = () => {
	mongoose
		.connect(process.env.MONGO_URI, {
			useNewUrlParser: true,
			useCreateIndex: true,
			useFindAndModify: false,
			reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
			reconnectInterval: 500, // Reconnect every 500ms
			useUnifiedTopology: true
		})
		.then(() => console.log('DB connected'))
		.catch(err => {
			console.error(err);
			console.log('RETRY CONNECTION DB');
			setTimeout(() => connectDB(), 5000);
		});
};

connectDB();

app.get('/v1/recipes', async (req, res) => {
	const result = await resolvers.Query.getRecipes();
	return res.json(result);
});

app.get('/v1/recipe/:id', async (req, res) => {
	const result = await resolvers.Query.getRecipe(req.params.id);
	return res.json(result);
});

app.post('/v1/recipe', async (req, res) => {
	const result = await resolvers.Query.addRecipe(req.body);
	return res.json(result);
});

app.put('/v1/recipe', async (req, res) => {
	const result = await resolvers.Query.updateRecipe(req.body);
	return res.json(result);
});

app.delete('/v1/recipe/:id', async (req, res) => {
	const result = await resolvers.Query.deleteRecipe(req.params.id);
	return res.json(result);
});

app.listen(process.env.PORT, () =>
	console.log(`Server listening on port ${process.env.PORT}!`)
);
