const Recipe = require('./models/Recipe');

module.exports = {
	Query: {
		getRecipes: async () => {
			const recipes = await Recipe.find({}).sort({ creationDate: 'desc' });
			return recipes;
		},
		getRecipe: async id => {
			const recipe = await Recipe.findById(id);
			return recipe;
		},
		searchPosts: async searchTerm => {
			if (searchTerm) {
				const searchResults = await Recipe.find(
					// Perform text search for search value of 'searchTerm'
					{ $text: { $search: searchTerm } },
					// Assign 'searchTerm' a text score to provide best match
					{ score: { $meta: 'textScore' } }
					// Sort results according to that textScore (as well as by likes in descending order)
				)
					.sort({
						score: { $meta: 'textScore' }
					})
					.limit(5);
				return searchResults;
			}
		},
		addRecipe: async ({
			name,
			image,
			vegetarian,
			people,
			ingredients,
			instructions
		}) => {
			const newRecipe = await new Recipe({
				name,
				image,
				vegetarian,
				people,
				ingredients,
				instructions
			}).save();
			return newRecipe;
		},
		updateRecipe: async ({
			id,
			name,
			image,
			vegetarian,
			people,
			ingredients,
			instructions
		}) => {
			const recipe = await Recipe.findOneAndUpdate(
				// Find post by postId and createdBy
				{ _id: id },
				{
					$set: { name, image, vegetarian, people, ingredients, instructions }
				},
				{ new: true }
			);
			return recipe;
		},
		deleteRecipe: async id => {
			const recipe = await Recipe.findOneAndRemove({ _id: id });
			return recipe;
		}
	}
};
