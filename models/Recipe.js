const mongoose = require('mongoose');

const RecipeSchema = new mongoose.Schema({
	id: {
		type: mongoose.Schema.Types.ObjectId
	},
	name: {
		type: String,
		required: true
	},
	image: {
		type: String
	},
	vegetarian: {
		type: Boolean
	},
	people: {
		type: Number
	},
	creationDate: {
		type: Date,
		default: Date.now
	},
	ingredients: {
		type: [String]
	},
	instructions: {
		type: String,
		required: true
	}
});

// Create index to search
RecipeSchema.index({
	'$**': 'text'
});

module.exports = mongoose.model('Recipe', RecipeSchema);
